/* Angular Main
   @author: Aaron J
   @purpose: trial test
 */
 
   var app = angular.module("ajTrial", ["ngRoute"]);
   
            app.config(function($routeProvider) {
                $routeProvider
                .when("/", {
                    templateUrl : "/wp-content/themes/bootstrap-basic/firstname.html"
                })
                .when("/contact", {
                    templateUrl : "/wp-content/themes/bootstrap-basic/contact.html",
					 resolve:{
							"check":function(ajFactoryTrial,$location){ 
								if(ajFactoryTrial.correctGoAhead()){   
								   
								}else{
									$location.path('/');           
									alert("Sorry you cannot directly access this page.");
								}
							}
						}
                })
            }); //Route
			
			app.factory("ajFactoryTrial",function(){
				var obj = {}
				this.gonow = false;
				obj.goAhead = function(){    
					this.gonow = true;
				}
				obj.correctGoAhead = function(){
					return this.gonow;          
				}
				return obj;
			}); //Factory
			
			
			app.controller('ajController', function($scope, $location, ajFactoryTrial){
				$scope.validNow = function(isFormValid){
				   if(isFormValid) {	//If first form is valid then go ahead go to 2nd view
					  ajFactoryTrial.goAhead();  
					  $location.path('/contact');  
				   }
				}
			}); //Controller