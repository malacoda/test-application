<?php
/* Aaron Jefferson Post Type testing */

get_header(); ?>

	<div ng-app="ajTrial">

         <form name="registerForm" id="signup-form">   
            <div ng-view></div>
         </form>   
         
         <div class="row" ng-controller="ajController">   
           <button type="button" class="btn btn-primary" ng-click="validNow(registerForm.$valid)"> Next >> </button> 
         </div>  
            
    </div>
    


<?php get_footer(); ?>
